
const path = require("path");
const fs = require("fs");

// autoImportComponents()
function autoImportComponents(options) {
    let filePath = options.filePath || null
    let name = options.name || getFileName(filePath);
    let isPage = filePath.includes('pages')
    let isSection = filePath.includes('sections')
    let isComponent = filePath.includes('components')
    let commonJsFile = path.resolve(__dirname, '../dev/js/common.js')
    let mixinPugFile = path.resolve(__dirname, '../dev/pages/mixins.pug')

    if (isComponent) {
        if (options.delete) {
            removeFromFile(commonJsFile, name, 'components')
            removeFromFile(mixinPugFile, name, 'components')
        } else {
            appendToFile({
                file: commonJsFile, 
                type: 'js',
                endLine: '// End: Components', 
                name: name, 
                folder: 'components', 
            })
            appendToFile({
                file: mixinPugFile,
                type: 'pug',
                endLine: '//- End: Mixins', 
                name: name, 
                folder: 'components', 
            })
        }
    } else if (isPage) {
        if (options.delete) {
            removeFromFile(commonJsFile, name, 'pages')
        } else {
            appendToFile({
                file: commonJsFile,
                type: 'js',
                endLine: '// End: Pages', 
                name: name, 
                folder: 'pages', 
            })
        }
    } else {
        if (options.delete) {
            removeFromFile(commonJsFile, name, 'sections')
        } else {
            appendToFile({
                file: commonJsFile,
                type: 'js',
                endLine: '// End: Sections', 
                name: name, 
                folder: 'sections', 
            })
        }
    }
}

function appendToFile(obj) {
    let file = obj.file;
    let name = obj.name;
    let folder = obj.folder;
    let endLine = obj.endLine
    let importKey = obj.type === 'pug' ? 'include' : 'import'
    let pathAsString = obj.type === 'pug' ? false : true
    let commonJsData = fs.readFileSync(file)
        .toString()
        .split("\n")
        .map( elm => {
            return elm.replace('\r', '')
        }) // Use map on Windows/Linux to remove carriage return \r
    let lineIndex = commonJsData.indexOf(endLine);
    
    let finalData;
    
   
    if (pathAsString) {
        commonJsData.splice(lineIndex, 0, `${importKey} \'../${folder}/${name}/${name}\'`)
    } else {
        commonJsData.splice(lineIndex, 0, `${importKey} ../${folder}/${name}/${name}`)
    }

    finalData = commonJsData.join('\n')

    fs.writeFile(file, finalData, function (err) {
        if (err) {
            throw err;
        }
    })

}

function removeFromFile(file, name, folder) {
    let stringToFind = `${folder}/${name}/${name}`
    let commonJsData = fs.readFileSync(file).toString().split("\n");
    let finalData;

    commonJsData.forEach((string,i) => {
        if(string.includes(stringToFind)) {
            commonJsData.splice(i, 1)
        }
    })

    finalData = commonJsData.join('\n')

    fs.writeFileSync(file, finalData, function (err) {
        if (err) {
            throw err
        }
    })

}

function getFileName(path) {
    return path.replace(/^.*[\\\/]/, "");
}




module.exports = autoImportComponents