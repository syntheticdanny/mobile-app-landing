const pug = require('pug');
const fs = require('fs');
const path = require('path');
const glob = require('glob');
 

module.exports = function () {
    let promise = new Promise(function ( resolve, reject ) {
        console.log('pug2Html => Start');
        initPug2Html(resolve)
        
    })

    return promise;
}

function initPug2Html(resolve) {
    const allRegs = [
        {
            // Img
            regExp: /src=\"(?:[^\"\/]*\/)*([^\"]+)(jpg|svg|jpeg|png|gif|pdf)\"/gi,
            newPath: `src="img/$1$2"`
        },
        {
            // Srcset
            regExp: /\"((?:[^\"\/]*\/)*([^\"]+)(jpg|svg|jpeg|png|gif|pdf)\s(\dx))(,\s((?:[^\"\/]*\/)*([^\"]+)(jpg|svg|jpeg|png|gif|pdf)\s(\dx)))\"/gi,
            newPath: `"img/$2$3 $4, img/$7$8 $9" `
        },
        {
            // SVG sprite
            regExp: /xlink:href=\"(?:[^\"\/]*\/)*([^\"]+)(svg#)(?:[^\"\/]*\/)*([^\"]+)\"/gi,
            newPath: `xlink:href="img/$1svg#$3"`
        },
        {
            // Video
            regExp: /src=\"(?:[^\"\/]*\/)*([^\"]+)(ogg|mp4|webm)"/gi,
            newPath: `src="video/$1$2"`
        },
        {
            // Video poster
            regExp: /poster=\"(?:[^\"\/]*\/)*([^\"]+)(jpg|svg|jpeg|png|gif|pdf)\"/gi,
            newPath: `poster="video/$1$2"`
        },
        {
            // Background image
            regExp: /url\(\"?(?:[^\"\/]*\/)*([^\"]+)(jpg|svg|jpeg|png|gif|pdf)([\'\"])?/gi,
            newPath: `url('img/$1$2'`
        },
        {
            // Link
            regExp: /href=\".*\/img\/(?:[^\"\/]*\/)*([^\"]+)(jpg|svg|jpeg|png|gif|pdf)\"/gi,
            newPath: `href="img/$1$2"`
        },
        {
            // Favicon
            regExp: /href=\".*\/assets\/favicon(?:[^\"\/]*\/)*([^\"]+)(jpg|svg|jpeg|png|gif|pdf|json)\"/gi,
            newPath: `href="favicon/$1$2"`
        },
    ]

    glob('./dev/pages/**/*.pug', {ignore: ['./dev/pages/layout.pug', './dev/pages/mixins.pug']}, function (err, filePath) {
        filePath.forEach( oneFile => {
            let rendered = pug.renderFile(oneFile, {pretty: true});
            let result = rendered;
            let oneFielName = getFileName(oneFile)

            allRegs.forEach(data => {

                result = result.replace(data.regExp, data.newPath)

            });

            fs.writeFile(path.resolve(`build/${oneFielName}.html`), result, 'utf8', function (err) {
                if (err) return console.log(err);
            });

        });

        resolve('pug2html => COMPLETE 🙌');
    })


    function getFileName(filePath) {
        return filePath.replace(/^.*[\\\/]/, '').split('.')[0]
    }
}


