var path = require('path');
var fs = require('fs');
var chokidar = require('chokidar');



var SVGSpriter = require('svg-sprite'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    fs = require('fs'),
    File = require('vinyl'),
    cwd = path.resolve('dev'),
    glob = require('glob');





module.exports = function (production) {
    let promise = new Promise(function ( resolve, reject ) {
        console.log('SVG sprite => Start');
        initSvgSprite(production, resolve)

    })

    return promise;
}


function initSvgSprite (production, resolve) {
    
    var config = {
        "shape": {
            "id": {
                "generator": function (name) { 
                    var slash = name.match(/\\/g);
                    var realSlash = null;
                    if (slash) {
                        realSlash = '\\'
                    } else {
                        realSlash = '/'
                    }
                    var pathArr = name.split(realSlash)
                    var icName = pathArr[pathArr.length - 1].split('.')[0]

                    // Filter only iocns with 'ic-' in name
                    if (icName.match(/ic-/i)) {
                        return icName
                    }
                    
                }
            }
        },
        "svg": {
            "doctypeDeclaration": false,
            "namespaceClassnames": false
        },
        "mode": {
            "symbol": {
                "dest": "dev/assets/img",
                // "dest": "build/img",
                "sprite": "sprite.min.svg",
                "inline": true,
                "example": false
            }
        }
    }

    var spriter = null
    var watcher;

    if (production) {
        buildSprite()
    } else {
        initWatcher()
    }

    function initWatcher() {
        watcher = chokidar.watch('dev/**/ic-*.svg', { 
            ignored: ['dev/assets/favicon/**', 'dev/assets/img/sprite.min.svg'],
            ignoreInitial: true
        })


        watcher
            .on('ready', function () {
                buildSprite()
                watcher.on('add', buildSprite)
            })
            .on('change', buildSprite)
            .on('unlink', buildSprite)
    }



    function buildSprite(){
        // console.log('build svg sprite');

        var spriter = new SVGSpriter(config);

        glob.glob('**/ic-*.svg', {
                cwd: cwd,
                ignore: ['assets/favicon/**', 'assets/img/sprite.min.svg', 'assets/img/svg']
            }, 
            function (err, files) {
                files.forEach(function (file) {
                    // Create and add a vinyl file instance for each SVG
                    spriter.add(new File({
                        path: path.join(cwd, file),							// Absolute path to the SVG file
                        base: cwd,											// Base path (see `name` argument)
                        contents: fs.readFileSync(path.join(cwd, file))		// SVG file contents
                    }));
                })

                spriter.compile(function (error, result, cssData) {

                    // Run through all configured output modes
                    
                    for (var mode in result) {

                        // Run through all created resources and write them to disk
                        for (var type in result[mode]) {
                            mkdirp.sync(path.dirname(result[mode][type].path));
                            fs.writeFileSync(result[mode][type].path, result[mode][type].contents);
                        }
                    }
            });
        });
        resolve('SVG sprite => COMPLETE 🙌');

    }

}