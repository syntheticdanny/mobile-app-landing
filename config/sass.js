const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');

module.exports = function(mode) {
	let resultFileName = 'css/[name].css';

	if (mode === 'single') {
		resultFileName = 'css/common.css';
	}

	return {
		module: {
			rules: [
				{
					test: /\.css$/,
					use: ExtractTextPlugin.extract({
						publicPath: '../',
						fallback: 'style-loader',
						use: [
							{
								loader: 'css-loader'
							}
						]
					})
				},
				{
					test: /\.scss$/,
					use: ExtractTextPlugin.extract({
						publicPath: '../',
						fallback: 'style-loader',
						use: [
							{
								loader: 'css-loader',
								options: {
									// sourceMap: true              // For faster build speed
								}
							},
							
							{
								loader: 'postcss-loader',
								options: {
									// sourceMap: 'inline',         // For faster build speed
									plugins: () => [require('autoprefixer')]
								}
							},
							{
								loader: 'sass-loader',
								options: {
									data:
										'@import "_var.scss"; @import "_mixins.scss"; @import "_settings.scss";',
									includePaths: [
										path.resolve(__dirname, '../dev/scss/')
									]
									// sourceMap: true,             // For faster build speed
									// sourceMapContents: true
								}
							}
							// {
							//     loader: 'sass-resources-loader',
							//     options: {
							//         resources: [
							//             './dev/scss/_mixins.scss',
							//             './dev/scss/_var.scss'
							//         ]
							//     },
							// }
						]
					})
				},
				{
					test: /\.(woff|woff2|eot|ttf|TTF|OTF|otf)$/,
					loader: 'file-loader?name=fonts/[name].[ext]'
				}
			]
		},
		plugins: [new ExtractTextPlugin(resultFileName)]
	};
};
