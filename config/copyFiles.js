var path = require('path');
var fs = require('fs');
var glob = require('glob');


module.exports = function (production) {
    let promise = new Promise(function ( resolve, reject ) {
        console.log('Copy files => Start');
        // initWatchers(production, resolve)
        copyAllFiles(resolve);
        
    })

    return promise;
}

function copyAllFiles(resolve) {
    // Copy images
    glob(
        'dev/**/*.{jpg,jpeg,png,svg,gif}', 
        {
            ignore: ['dev/assets/favicon/**', 'dev/assets/img/sprite.min.svg']
        },
        function (err, files) {
            files.forEach((file, index, arr) => {
                // console.log('copy img');
                copySingleFile(file)

                if (index === arr.length - 1) {
                    resolve('Copy files => COMPLETE 🙌')
                }
            });
            
        }
    )

    // Copy favicons
    glob(
        'dev/assets/favicon/**/*.*', 
        {},
        function (err, files) {
            files.forEach(file => {
                // console.log('copy favicon');
                copySingleFile(file, 'favicon')
            });
        }
    )

    // Copy video
    glob(
        'dev/assets/video/**/*.*', 
        {},
        function (err, files) {
            files.forEach(file => {
                // console.log('copy favicon');
                copySingleFile(file, 'video')
            });
        }
    )

    
}



function copySingleFile(filePath, folder) {
    var fileName = getFileName(filePath)
    var inStr = fs.createReadStream(filePath);
    var buildPath = path.resolve(`build/${folder ? folder : 'img'}/`)

    // Create img filder if doesn't exist
    if (!fs.existsSync(buildPath)){
        fs.mkdirSync(buildPath);
    }

    var outStr = fs.createWriteStream(buildPath + '/' + fileName);

    inStr.pipe(outStr);
}

function getFileName(filePath) {
    return filePath.replace(/^.*[\\\/]/, '')
}


