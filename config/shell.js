const WebpackShellPlugin = require('webpack-shell-plugin');

module.exports = function (env) {
    return {
        plugins: [
            new WebpackShellPlugin({
                onBuildStart: ['echo "Start building..."'],
                onBuildEnd: [`node server.js ${env.target} ${env.mode}`]
            })
        ]
    }
}