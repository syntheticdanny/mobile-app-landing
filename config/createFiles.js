const path = require("path");
const fs = require("fs");
const chokidar = require("chokidar");
var autoImport = require('./autoImport')

const componentTemplate =
    `mixin #{name}(config)
    -
        const defaults = {

        }
    
    -   const params = Object.assign({}, defaults, config)
    
    .#{name}&attributes(attributes)
`;

const sectionTemplate = `
section.#{name}
    
`;

const pageTemplate =
    `extends ../layout.pug
block title
    title #{name}
block main                   
`;

const singleFilePageTemplate = 
`extends ../layout.pug

block main                   
`;

const markdownTemplate = 
`
##{name}

Enter description here...

##PUG
######Defaults
\`\`\`

\`\`\`

####Example
\`\`\`

\`\`\`

**or**
\`\`\`

\`\`\`


####Options
| Variable      | Type             | Default                          | Description  |
| ------------- |:----------------:| --------------------------------:| ------------:|
| name          | type             | value                            | Comment      |
| name          | type             | value                            | Comment      |   
`;

module.exports = (modeSingle) => {
    const watchPaths = ["dev/pages/", "dev/sections/", "dev/components"];
    const templates = {
        component: componentTemplate,
        section: sectionTemplate,
        page: modeSingle ? singleFilePageTemplate : pageTemplate,
        md: markdownTemplate,
    };

    initWatchers();

    function initWatchers() {
        const watcher = chokidar.watch(watchPaths, {
            ignored: /(^|[\/\\])\../,
            ignoreInitial: true,
        });

        watcher.on("addDir", filePath => {
            const types = Object.keys(templates);
            const neededTypes = types.filter(type => {
                return filePath.match(new RegExp(type, "g"));
            });

            const type = neededTypes.length ? neededTypes[0] : false;

            if (type) {
                createNewFile(filePath, type);
            }
        });

        watcher.on("unlinkDir", filePath => {
            // Auto import components
            autoImport({
                delete: true,
                filePath: filePath
            })
        });
    }

    function createNewFile(filePath, type) {
        console.log( type );
        const name = getFileName(filePath);

        // Auto import components
        autoImport({
            name: name,
            filePath: filePath
        })
        
        // Exit function if files exists
        if (fs.readdirSync(filePath).length > 0) {
            return
        }
       
        const content = {
            pug: getPugTemplate(name, type),
            scss: getScssContent(name, type),
            js: getJsContent(name, type),
            md: getMdContent(name, type)
        }

        // Create .pug
        if ( !fs.existsSync(path.resolve(filePath) + `/${name}.pug`) ) {
            fs.writeFile(path.resolve(filePath) + `/${name}.pug`, content.pug, err => {
                if (err) throw err;
            });
        }

        // Create .md
        if ( !fs.existsSync(path.resolve(filePath) + `/${name}.md`) && type === 'component' ) {
            fs.writeFile(path.resolve(filePath) + `/${name}.md`, content.md, err => {
                if (err) throw err;
            });
        }
        

        // Create .scss
        if ( !fs.existsSync(path.resolve(filePath) + `/${name}.scss`) ) {
            fs.writeFile(path.resolve(filePath) + `/${name}.scss`, content.scss, err => {
                if (err) throw err;
            });
        }
        

        // Create .js
        if ( !fs.existsSync(path.resolve(filePath) + `/${name}.js`) ) {
            fs.writeFile(path.resolve(filePath) + `/${name}.js`, content.js, err => {
                if (err) throw err;
            });
        }
    }

    function getPugTemplate(name, type) {
        const template = templates[type].replace(/#{name}/g, name);

        return template;
    }

    function getMdContent(name, type) {
        const template = markdownTemplate.replace(/#{name}/g, name.charAt(0).toUpperCase() + name.slice(1));

        return template;
    }

    function getScssContent(name, type) {
        let content;
        switch (type) {
            case 'component':
            case 'section':
                content = `.${name} {}`;
                break;
            default:
                content = '';
                break;
        }
        return content;
    }

    function getJsContent(name, type) {
        let content;
        switch (type) {
            case 'component':
            case 'section':
            case 'page':
            default:
                content = `import './${name}.scss';`;
                break;
        }
        return content;
    }

    function getFileName(path) {
        return path.replace(/^.*[\\\/]/, "");
    }
}