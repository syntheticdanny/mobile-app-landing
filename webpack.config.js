const path = require('path');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin');
const merge = require('webpack-merge');
const webpack = require('webpack');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');

// const WebpackShellPlugin = require('webpack-shell-plugin');
const entry = require('./config/entrys')
const sass = require('./config/sass')
const sassMin = require('./config/sass.min')
const jsUglify = require('./config/js-uglify')
const images = require('./config/images')
const shell = require('./config/shell')
// console.log(shell);


const PATHS = {
    dev: path.join(__dirname, 'dev'),
    build: path.join(__dirname, 'build')
};

console.log(entry);



// // Common cofig
const commonConfig = merge([{
        entry: entry,
        output: {
            path: PATHS.build,
            filename: 'js/[name].js',
            publicPath: '/'
        },
        resolve: {
            alias: {
                components: PATHS.dev + '/components/',
                fonts: PATHS.dev + '/fonts',
            }
        },
        // devtool: 'cheap-eval-source-map',
        module: {
            rules: [{
                test: /\.js$/,
                exclude: /node_modules'/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015'],
                        cacheDirectory: true
                    }
                }
            }]
        },
        plugins: [
            new CleanWebpackPlugin(['build']),
            new FriendlyErrorsWebpackPlugin(),
            new HardSourceWebpackPlugin(),
            new webpack.optimize.CommonsChunkPlugin({
                name: 'common',
                minChunks: 2
            }),
            new CopyWebpackPlugin([{
                    context: PATHS.dev,
                    from: './js/vendors/**/*.*',
                    to: PATHS.build
                },
                {
                    context: PATHS.dev,
                    from: './js/vendors/jquery.js',
                    to: PATHS.build + '/js/vendors/jquery.js'
                },
                {
                    context: PATHS.dev,
                    from: './js/backend/backend.js',
                    to: PATHS.build + '/js/backend/backend.js'
                },
                {
                    context: PATHS.dev + '/assets',
                    from: './data/**/*', // Copy JSON data files
                    to: PATHS.build + '/'
                },
            ]),
        ]
    },
    images()
]);

// Single file config
const singlFileConfig = merge([{
        entry: PATHS.dev + '/js/common.js',
        output: {
            path: PATHS.build,
            filename: 'js/common.js',
            publicPath: '/'
        },
        resolve: {
            alias: {
                components: PATHS.dev + '/components/',
                fonts: PATHS.dev + '/fonts'
            }
        },
        module: {
            rules: [{
                    test: /\.yaml$/,
                    use: [{
                            loader: 'json-loader'
                        },
                        {
                            loader: 'yaml-loader'
                        }
                    ]
                },
                {
                    test: /\.js$/,
                    exclude: /node_modules'/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['es2015']
                        }
                    }
                }
            ]
        },
        plugins: [
            new CleanWebpackPlugin(['build']),
            new FriendlyErrorsWebpackPlugin(),
            new HardSourceWebpackPlugin(),
            // new webpack.optimize.CommonsChunkPlugin({
            //     name: 'common',
            //     minChunks: 2
            // }),
            new CopyWebpackPlugin([{
                    context: PATHS.dev,
                    from: './js/vendors/**/*.*',
                    to: PATHS.build
                },
                {
                    context: PATHS.dev,
                    from: './js/vendors/jquery.js',
                    to: PATHS.build + '/js/vendors/jquery.js'
                },
                {
                    context: PATHS.dev,
                    from: './js/backend/backend.js',
                    to: PATHS.build + '/js/backend/backend.js'
                },
                {
                    context: PATHS.dev + '/assets',
                    from: './data/**/*', // Copy JSON data files
                    to: PATHS.build + '/'
                },
            ]),
        ]
    },
    images()
]);

// Only for css recompiling
const cssOnlyConfig = merge([{
        entry: entry,
        output: {
            path: PATHS.build,
            filename: 'js/[name].js',
            publicPath: '/'
        },
        resolve: {
            alias: {
                components: PATHS.dev + '/components/',
                fonts: PATHS.dev + '/fonts'
            }
        },
        module: {
            rules: [

            ]
        },
        plugins: [
            // new CleanWebpackPlugin(['build']),
            new FriendlyErrorsWebpackPlugin(),
            new HardSourceWebpackPlugin(),
            new webpack.optimize.CommonsChunkPlugin({
                name: 'common',
                minChunks: 2
            }),

        ]
    },
    images()
]);

// Documentation cofig
const documentationConfig = merge([{
        entry: entry,
        output: {
            path: PATHS.build,
            filename: 'js/[name].js',
            publicPath: '/'
        },
        resolve: {
            alias: {
                components: PATHS.dev + '/components/',
                fonts: PATHS.dev + '/fonts',
            }
        },
        // devtool: 'cheap-eval-source-map',
        module: {
            rules: [{
                test: /\.js$/,
                exclude: /node_modules'/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015'],
                        cacheDirectory: true
                    }
                }
            }]
        },
        plugins: [
            new CleanWebpackPlugin(['build']),
            new FriendlyErrorsWebpackPlugin(),
            new HardSourceWebpackPlugin(),
            // new webpack.optimize.CommonsChunkPlugin({
            //     name: 'common-docs',
            //     minChunks: 2
            // }),
            new CopyWebpackPlugin([{
                    context: PATHS.dev,
                    from: './js/vendors/**/*.*',
                    to: PATHS.build
                },
                {
                    context: PATHS.dev,
                    from: './js/vendors/jquery.js',
                    to: PATHS.build + '/js/vendors/jquery.js'
                },
                {
                    context: PATHS.dev,
                    from: './js/backend/backend.js',
                    to: PATHS.build + '/js/backend/backend.js'
                },
                {
                    context: PATHS.dev + '/assets',
                    from: './data/**/*', // Copy JSON data files
                    to: PATHS.build + '/'
                },
            ]),
        ]
    },
    images()
]);



// Export config, depending on --env
module.exports = (env) => {

    // console.log('env target', env.target);
    // console.log('env mode', env.mode);

    // Production : Single file
    if (env.target === 'production' && env.mode === 'single') {
        return merge([
            singlFileConfig,
            sassMin(env.mode),
            shell(env)
        ]);
    }

    // Production : Multiple files
    if (env.target === 'production') {
        return merge([
            commonConfig,
            jsUglify(),
            sassMin(),
            shell(env)
        ]);
    }

    // Development : Single file
    if (env.mode === 'single') {
        return merge([
            singlFileConfig,
            sass(env.mode),
            shell(env)
        ]);
    }

    // Css Only
    if (env.target === 'css') {
        return merge([
            cssOnlyConfig,
            sassMin(),
        ]);
    }

    // Documentation: Multiple files
    if (env.target === 'docs') {
        return merge([
            documentationConfig,
            sass(),
            shell(env)
        ]);
    }

    // Development : Multiple files
    return merge([
        commonConfig,
        sass(),
        shell(env)
        // devServer()
    ]);


};