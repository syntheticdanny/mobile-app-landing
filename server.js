var express = require('express');
var app     = express();
var fs = require('fs-extra');
var chokidar = require('chokidar');
var path = require('path')
var browserSync = require('browser-sync');
var production =  process.argv[2] == 'production' // comes from shell.js
var modeSingle = process.argv[3] === 'single' // comes from shell.js
const getPort = require('get-port');
var createFiles = require('./config/createFiles')
let pug2html = require('./config/pug2html')
var spr = require('./config/svg-sprite.js');

let debugMode = false;
if (debugMode) {
	console.log('----------> DEBUG MODE <-----------');
}

// Sprite create
if (!debugMode) {
}

if (production) {
	console.log('=============> PRODUCTION <===============');
	var copyFiles = require('./config/copyFiles.js');

	copyFiles(production)
		.then(
			function (result) {
				console.log(result);
				return spr(production)
			}
		)
		.then(
			function (result) {
				console.log(result);
				return pug2html()
			}
			
		)
		.then(
			(result) => { 
				console.log(result);
				console.log('===========> PRODUCTION DONE <=============');
			}
		)

	
} else {

	if (!debugMode) {
		spr()
	}
	
	browserSync.create()

	/**
	* Create pack of files (.pug, .js, .scss) when add new folder in pages or sections
	*/
	
	createFiles(modeSingle)


	// отключаем кеширование
	// app.disable('view cache');
	// указываем какой шаблонизатор использовать
	app.set('view engine', 'pug');
	// расположение шаблонов ('src/templates')
	app.set('views', './');
	// путь до наших стилей, картинок, скриптов и т.д.
	app.use(express.static(__dirname + '/build'));
	app.use(express.static(__dirname + '/dev'));
	app.use(express.static(__dirname + '/dev/assets'));
	app.use(express.static(__dirname + '/dev/sections/footer/'));		// Find image in sections/footee/logo-webus.svg

	// Add static files from all components/** 
	let componentDirs = dirs(__dirname + '/dev/components/')
	componentDirs.forEach(function (dir) {
		app.use(express.static(__dirname + `/dev/components/${dir}`));
	})


	// F: Detect dirrectories
	function dirs(p){
		return fs.readdirSync(p).filter(f => fs.statSync(path.join(p, f)).isDirectory())
	}

	// роутинг на наши страницы
	app.get('/:page?', function (req, res) {
		console.time('Pug')

		// Разбираемся с ошибкой Cannot find module 'ico'
		if (req.url == '/favicon.ico') {
			return false
		}

		var fileName = req.url.replace(/\//g, '') || 'index';
		var re = /(html)/g;
		var hasHtml = re.test(fileName);
		var pagePath = null

		// If link has .html in the end, then remove it
		if (hasHtml) {
			fileName = fileName.split('.')[0]
		}

		pagePath = './dev/pages/' + fileName + '/'

		if (fileName == 'home' || fileName == 'index') {
			res.render(pagePath + 'index')
		} else if (fileName.indexOf('?') !== -1) {
			res.redirect('search')
		} else {
			res.render(pagePath + fileName)
		}

		console.timeEnd('Pug')
	});

	// редирект на главную страницу
	app.get('/', function(req, res) {
		res.redirect('/index.html');
	});


	// Create random proxy port
	var port;
	if (!debugMode) {
		getPort()
		.then(port => {
			port = port;
			console.log('proxyPort: ' + port);

			var listener    = app.listen(port);
			startBs(port)
		});
	}
	


	function startBs(proxyPort) {
		// proxy на локальный сервер на Express
		browserSync.init({
			proxy: 'http://localhost:'   + proxyPort,
			startPath: '/',
			notify: false,
			tunnel: false,
			host: 'localhost',
			port: 3000,
			injectChanges: true,
			logPrefix: 'Proxy to localhost:' + proxyPort,
			reloadDelay: 100,
			reloadThrottle: 100,
			open: true,
			ghostMode: false
		});

		// обновляем страницу, если обновились assets файлы
		// обновляем страницу, если был изменен исходник шаблона
		browserSync.watch([
			'dev/pages/**/*.pug',
			'dev/components/**/*.pug',
			'dev/sections/**/*.pug',
			'build'
		]).on('change', browserSync.reload);

	}

}
