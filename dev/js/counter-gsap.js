import { TweenMax } from "gsap";

let CounterGSAP = function (elm, start) {

    let domElms = document.querySelectorAll(elm)
    let endNumber;

    for (var i = 0; i < domElms.length; i++) {
        endNumber = domElms[i].getAttribute('data-counter')
        initCounter(domElms[i], start, endNumber)
    }   
}


function initCounter(elm, start, end) {
    let endNum = { num: end }

    TweenMax.from(endNum, 2, { num: start, roundProps: 'num', onUpdate: updateDom, ease: Linear.easeNone })

    function updateDom() {
        elm.innerHTML = endNum.num.toLocaleString()
    }

}



export default CounterGSAP