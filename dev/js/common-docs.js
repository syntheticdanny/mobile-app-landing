// CSS
import '../scss/common.scss';

// Plugins

// Components
import '../components/button/button'
import '../components/header/header'
import '../components/nav/nav'
import '../components/dropdown/dropdown'
import '../components/iframe/iframe'
import '../components/modal/modal'
import '../components/icon/icon'
import '../components/form/form'
import '../components/accordion/accordion'
import '../components/tab/tab'
import '../components/gallery/gallery'
// End: Components

// Pages
import '../pages/docs/docs'
// End: Pages

// Sections
// import '../sections/footer/footer'
// End: Sections

// Add global App variable
if (!window.App) {
  window.App = {}
} 
