class SS {
    constructor(elmsArr) {
        for (let i = 0; i < elmsArr.length; i++) {
            this[i] = elmsArr[i]
        }
        this.length = elmsArr.length
    }

    on(method, elm, fn, options) {
        if (typeof elm === 'string') {
            // Delegate
            for (let i = 0; i < this.length; i++) {
                let target = S(this[i]).find(elm)
                if (target.length > 0) {
                    this[i].addEventListener(method, handleEvent.bind(this, fn, elm), options);   
                }
            }
        } else {
            // Not delegate
            let fn = elm;
            for (let i = 0; i < this.length; i++) {
                let elm = this[i]
                this[i].addEventListener(method, fn, options);
            }
    
        }

        function handleEvent(fn, elm, e) {
            let target = S(e.target).closest(elm)
            if (target.length) {
                fn.apply(target)
            }
        }
        
        return this
    }

    one(method, fn, options) {
        let dom = this;

        function proxy() {
            fn.apply(this)
            this.removeEventListener(method, proxy, options)
        }

        dom.on(method, proxy)

        return this
    }

    trigger(method) {
        var event = new CustomEvent(method);
        for (let i = 0; i < this.length; i++) {
            this[i].dispatchEvent(event)
        }
    }

    addClass(cls) {
        for (let i = 0; i < this.length; i++) {
            if (!this[i].classList.contains(cls)) {
                this[i].classList.add(cls);
            }
        }

        return this
    }

    removeClass(cls) {
        for (let i = 0; i < this.length; i++) {
            if (this[i].classList.contains(cls)) {
                this[i].classList.remove(cls);
            }
        }

        return this
    }

    toggleClass(cls) {
        for (let i = 0; i < this.length; i++) {
            this[i].classList.toggle(cls);
        }

        return this
    }

    hasClass(cls) {
        for (let i = 0; i < this.length; i++) {
            return this[i].classList.contains(cls);
        }
    }

    html(data) {
        if (data != undefined) {
            for (let i = 0; i < this.length; i++) {
                this[i].innerHTML = data;
            }
        } else {
            return this[0].innerHTML
        }

        return this
    }

    append(data) {
        for (let i = 0; i < this.length; i++) {
            this[i].insertAdjacentHTML('beforeend', data);
        }

        return this
    }

    prepend(data) {
        for (let i = 0; i < this.length; i++) {
            this[i].insertAdjacentHTML('afterbegin', data);
        }

        return this
    }

    before() {

    }

    after() {

    }

    eq(index) {
        if (typeof index !== 'number') return

        return new S(this[index])
    }

    index(){
        let index = [].slice.call(this[0].parentNode.children).indexOf(this[0])
        return index
    }

    attr(attr, data) {
        if (data) {
            // Set
            for (let i = 0; i < this.length; i++) {
                // Return if has attribute and attribute is equal to data
                if (this[i].hasAttribute(attr) && this[i].getAttribute(attr) === data) return

                this[i].setAttribute(attr, data)
            }
        } else {
            // Get
            let attrsArr = []
            for (let i = 0; i < this.length; i++) {
                attrsArr.push(this[i].getAttribute(attr))
            }

            if (attrsArr.length === 1) {
                return attrsArr[0]
            } else {
                return attrsArr
            }

        }

        return this
    }

    val(data) {
        if (data != undefined) {
            // Set
            for (let i = 0; i < this.length; i++) {
                this[i].value = data;
            }
        } else {
            // Get
            let valArr = []
            for (let i = 0; i < this.length; i++) {
                valArr.push(this[i].value)
            }

            if (valArr.length === 1) {
                return valArr[0]
            } else {
                return valArr
            }
        }


        return this
    }

    children() {
        let childrenArr = []
        let parents = []

        for (let i = 0; i < this.length; i++) {
            let chi = [].slice.call(this[i].children)
            childrenArr = childrenArr.concat(chi)
            parents.push(chi)
        }

        if (parents.length === 1) {
            return childrenArr
        }

        return {
            0: childrenArr,
            fragmets: parents,
            length: childrenArr.length
        }
    }

    find(selector) {
        let foundedItems = []
        for (let i = 0; i < this.length; i++) {
            let found = this[i].querySelectorAll(selector)

            for (let j = 0; j < found.length; j++) {
                foundedItems.push(found[j])

            }
        }

        return new SS(foundedItems)
    }

    siblings(selector) {
        return this.nextAll(selector).add(this.prevAll(selector))
    }

    closest(selector) {
        let closestElm;
        
        if (typeof selector === undefined || this.length === 0) {
            return new SS([]);
        }

        closestElm = this[0].closest(selector)
        // Returns only one closest element.
        // Maybe should return array of all closests?
        return S(closestElm);
    }

    remove() {
        for (let i = 0; i < this.length; i++) {
            this[i].remove()
        }

        return this
    }

    add(dom) {
        let thisLength = this.length;

        for (const key in dom) {
            if (dom.hasOwnProperty(key) && key !== 'length') {
                const element = dom[key];
                this[thisLength] = element
                thisLength++
            }
        }

        this.length = thisLength

        return this
    }

    nextAll(selector) {
        let nextElms = []
        let thisElm = this[0]

        while (thisElm.nextElementSibling) {
            if (typeof selector === 'string' && selector.length > 0) {
                // Is is selector and selectors length > 0
                if (S(thisElm.nextElementSibling).is(selector)) {
                    nextElms.push(thisElm.nextElementSibling)
                }
            } else {
                // All elements without any selector
                nextElms.push(thisElm.nextElementSibling)
            }
            thisElm = thisElm.nextElementSibling
        }

        return new SS(nextElms)
    }

    prevAll(selector) {
        let prevElms = []
        let thisElm = this[0]

        while (thisElm.previousElementSibling) {
            if (typeof selector === 'string' && selector.length > 0) {
                // Is is selector and selectors length > 0
                if (S(thisElm.previousElementSibling).is(selector)) {
                    prevElms.push(thisElm.previousElementSibling)
                }
            } else {
                // All elements without any selector
                prevElms.push(thisElm.previousElementSibling)
            }

            thisElm = thisElm.previousElementSibling
        }

        return new SS(prevElms)
    }

    is(selector) {
        let thisElm = this[0];
        let compareWith;
        let i;

        if (typeof selector === 'string') {
            if (thisElm.matches) {
                return thisElm.matches(selector)
            } else if (thisElm.webkitMatchesSelector) {
                return thisElm.webkitMatchesSelector(selector)
            } else if (thisElm.mozMatchesSelector) {
                return thisElm.mozMatchesSelector(selector)
            } else if (thisElm.msMatchesSelector) {
                return thisElm.msMatchesSelector(selector)
            }
        } else if (selector.nodeType || selector === window || selector === document) {
            compareWith = selector.nodeType ? [selector] : selector;
            for (i = 0; i < compareWith.length; i += 1) {
                if (compareWith[i] === thisElm) return true;
            }
            return false
        }

    }

    prop(props, value) {

        // Set
        if (typeof value === 'boolean') {
            for (let i = 0; i < this.length; i++) {
                this[i][props] = value
            }
            return this

        } else {
            // Get
            let arrOfValue = []
            for (let i = 0; i < this.length; i++) {
                arrOfValue.push(this[i][props])
            }

            if (arrOfValue.length === 1) {
                return arrOfValue[0]
            }

            return arrOfValue
        }



    }

    each(clbk) {
        if (!clbk) return this;

        for (let i = 0; i < this.length; i++) {
            clbk.call(this[i], this[i], i)
        }

        return this;
    }

    for (clbk) {
        if (!clbk) return this;

        for (let i = 0; i < this.length; i++) {
            clbk.call(this[i], S(this[i]), i)
        }

        return this;
    }
}

function SA(selector) {
    let elms = [];

    if (selector instanceof SS) {
        return selector
    }

    // null
    if (!selector) return new SS([])

    if (typeof selector === 'string' && selector.length > 0) {
        elms = [].slice.call(document.querySelectorAll(selector))
    } else if (selector.nodeType || selector === window || selector === document) {
        elms.push(selector)
    } else if (selector === '') {
        return new SS([])
    }
    return new SS(elms);
}

if (!window.SA) {
    window.SA = SA
}