// CSS
import '../scss/common.scss';

// Plugins

import Tweenmax from './vendors/gsap/TweenLite.min'

// Components
import '../components/button/button'
import '../components/header/header'
import '../components/nav/nav'
import '../components/dropdown/dropdown'
import '../components/iframe/iframe'
import '../components/modal/modal'
import '../components/icon/icon'
import '../components/form/form'
import '../components/accordion/accordion'
import '../components/tab/tab'
import '../components/gallery/gallery'
import '../components/btn2/btn2'
import '../components/ic2/ic2'
import '../components/listItems/listItems'
import '../components/dropdown-menu/dropdown-menu'
// End: Components

// Pages
import '../pages/index/index'
import '../pages/404/404'
import '../pages/no-ie/no-ie'
import '../pages/docs/docs'
import '../pages/design/design'
import '../pages/list/list'
// import '../pages/logo-list/logo-list'
// import '../pages/featured-list/featured-list'
import '../pages/drop/drop'
import '../pages/checkbox/checkbox'
// End: Pages

// Sections
import '../sections/footer/footer'
import utils from './vendors/utils';
// End: Sections

// Add global App variable
if (!window.App) {
  window.App = {}
}