// console.log('Backend');

S.on('.form', 'submit', function (e) {
    e.preventDefault();
    let thisForm = window.App.Validation(this, {prealoder: true})

    if (thisForm.isValid) {
        thisForm.startPreload()

        setTimeout(() => {
            thisForm.setSuccess()
        }, 2000);
    }
})

// console.log( App );