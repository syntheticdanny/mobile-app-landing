import './tab.scss';
import '../../components/scroll-x/scroll-x'

class Tab {
    constructor(parent, options) {
        var self = this;
        self.container = S.find(parent);
        self.btns = S.find(self.container, '.tab-btns__toggle-tab')
        self.tabItems = Array.from(self.container[0].querySelectorAll('.tab__item'))
        self.activeIndex = 0;

        Object.assign(this, options)

        if (this.tabItems.length == 0) {
            self.tabItems = S.find(this.itemsContainer, '.tab__item')
        }

        S.on(self.btns, 'click', self.setActiveTab.bind(this))

        if (self.activeIndex > 0) {
            S(self.btns[self.activeIndex]).trigger('click')
        }
    }

    setActiveTab(e) {
        let clickedBtn = e.target.closest('.tab-btns__toggle-tab')
        let index = [...this.btns].indexOf(clickedBtn)

        // Change button
        this.btns[this.activeIndex].classList.remove('js-tab-active')
        this.btns[index].classList.add('js-tab-active')

        // change items
        this.tabItems[this.activeIndex].classList.remove('js-tab-active')
        this.tabItems[index].classList.add('js-tab-active')

        this.activeIndex = index
    }
}
// var toggleBtn = document.querySelectorAll('.toggle-tab')
// var firstTab = document.querySelector('.my-item')

// var toggleBtn = document.querySelectorAll('.toggle-tab')
// var firstTab = document.querySelector('.my-item')

// window.onload = function () {
//     firstTab.classList.add('js-opened-tab');
//     var h = firstTab.offsetHeight;
//     var container = document.querySelector('#container');
//     container.style.height=h+"px";
// }
// for(var i = 0; i<toggleBtn.length; i++) {
//     toggleBtn[i].addEventListener('click', function(e) {
//         e.preventDefault();
//         var openedTab = document.querySelector('.js-opened-tab');              
//         openedTab.classList.remove('js-opened-tab')
//         var id = this.getAttribute('data-index');        
//         var item = document.getElementById(id);       
//         item.classList.add('js-opened-tab');
//         openedTab = document.querySelector('.js-opened-tab');
//         var p = openedTab.clientHeight;
//         container.style.height=p+"px";           
//     })
// }

export default Tab


if(S.find('.tab').length > 0) {
    S.each('.tab', function () {
        var f = new Tab(this, {
            activeIndex: this.getAttribute('data-tab-activeIndex'),
            itemsContainer: '.other-place-tab-container'
        })
    
    })
    
}
