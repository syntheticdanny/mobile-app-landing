import './accordion.scss'
import './accordion.custom.scss'

import {TweenMax, TimelineMax} from 'gsap';

class Accordion{
    constructor(selector, options){
        var def = {
            speed: .3,
            collapse: false
        }

        Object.assign(this, def, options)
        this.parent = selector
        this.collapse = this.parent.getAttribute('data-collapse') ? true : false

       this.parent.addEventListener('click', this.clickHandler.bind(this))
    }

    clickHandler(e){
        e.stopPropagation()
        let target = e.target.closest('.accordion__title')
        if (target) {
            this.toggleItem(target)
        }
    }

    toggleItem(elm){
        if (elm.classList.contains('active')) {
            S.slideClose(elm.nextElementSibling)
            elm.classList.remove('active')
        } else {
            S.slideOpen(elm.nextElementSibling)
            elm.classList.add('active')
        }
    }

    // TODO: fix this logic...
    collapseOther(clickedElm){
        for (const key in this.items) {
            const item = this.items[key];
            if (item !== clickedElm) {
                TweenMax.to(item.content, this.speed, {height: 0})
                item.isActive = false
                item.title.removeClass('show')
            }
        }
    }
}

initAccordions();
function initAccordions() {
    S.each('.accordion', function (el) {
        new Accordion(this)
    })    
}


// utils.makeGlobal('initAccordions', initAccordions)
