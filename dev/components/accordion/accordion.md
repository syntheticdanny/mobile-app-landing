#Accordion

This is accordion.

##PUG
######Defaults
```pug
    {
      collapse: false,
      items: [
        {
          title:'privet',
          text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam quas, accusamus reprehenderit, magnam quidem dolores voluptas eveniet deleniti ex maxime quis sint. Commodi neque placeat cum recusandae, dolores obcaecati exercitationem doloremque debitis repellendus consectetur, ipsam quis reprehenderit itaque illo eos atque, a ipsa consequuntur! Maxime dolor, temporibus, commodi, facilis natus culpa architecto corporis aperiam impedit quae soluta minima id sint et molestias vitae consequatur ratione inventore. Rerum velit minus assumenda quisquam eligendi, eum quae ipsa natus omnis cumque facilis iure explicabo. Culpa itaque incidunt accusantium pariatur, adipisci quasi ut sapiente, repellendus voluptatem dolorum placeat omnis laborum nesciunt quas dicta eum.'
        },
        {
          title:'hello world',
          text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi, tempora eos rerum nihil eligendi placeat recusandae facere cumque quia excepturi sit voluptate! Sint, fuga assumenda iusto ex est ea, sunt unde fugit, eligendi quia saepe odio similique quo alias magnam doloremque adipisci quasi minus soluta atque. Sit perspiciatis temporibus molestias?'
        }, {
          title:'end of mass',
          text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi esse ipsam, voluptate in, perferendis nemo eos earum nisi tempore asperiores neque officiis voluptates commodi quasi rerum voluptatibus, nostrum maiores sequi fuga voluptas doloribus quidem nulla non praesentium? Nobis enim distinctio expedita quia repellendus dolorem numquam suscipit, recusandae at veritatis! Odio, corporis! Sapiente, labore maxime? Iusto commodi iure tempore molestias modi non maxime voluptates, ipsam dolore explicabo saepe, earum, repellat repellendus asperiores magni? Velit a ut sunt deleniti optio amet cum mollitia at culpa omnis accusantium enim quaerat qui repellendus, sequi ea. Accusamus suscipit eum aperiam! Quibusdam facere cupiditate doloribus? Pariatur consectetur numquam, eaque voluptatibus doloribus accusantium ducimus possimus blanditiis magni vitae molestiae odio mollitia quos quisquam molestias voluptatem tenetur hic inventore, rerum ipsa voluptates quo ea nemo et! Quaerat dicta non nemo deserunt illum, nihil officia animi, facilis doloribus iure molestias! Doloremque quo beatae provident ipsa voluptas a odit praesentium dolorem! Iusto commodi unde aliquam, numquam nisi autem quam voluptas exercitationem culpa sint nemo? Ducimus aliquid doloribus repellat culpa, commodi distinctio dolores doloremque quo ad id ex iusto nemo explicabo obcaecati? Atque odio eveniet vero! Corrupti nobis necessitatibus quisquam, iure eveniet nihil! Assumenda modi quas libero nam velit, dignissimos mollitia!'
        }
      ]
    }
```

####Example
```pug
    +accordion
```

**or**
```pug
    +accordion
        +accordion-item({title: 'Some title 1'})
            p Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae, corrupti ipsum aliquid minima maiores sapiente quod odit eum iure numquam.
        +accordion-item({title: 'Some title 2'})
            p Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae, corrupti ipsum aliquid minima maiores sapiente quod odit eum iure numquam.
        +accordion-item({title: 'Some title 3'})
            p Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae, corrupti ipsum aliquid minima maiores sapiente quod odit eum iure numquam.

```


####Options
| Variable      | Type             | Default                          | Description  |
| ------------- |:----------------:| --------------------------------:| ------------:|
| collapse      | Boolean          | true | Придумать текст           |
| items         | Array            | [{title: '', text: '}, {}, ...]  |   See above |