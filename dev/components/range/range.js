import './nouislider/nouislider.min.scss';
import './range.scss';
import noUiSlider from './nouislider/nouislider.min';

function rangeSlider(range, obj) {
    
    var parent = range
    var minValueInput = parent.querySelector('.range__min-val')
    var maxValueInput = parent.querySelector('.range__max-val')
    var sliderElm = parent.querySelector('.range__slider')
    var sliderType = obj.type
    var step = +(obj.step)
    var minValue = +(obj.min)
    var maxValue = +(obj.max)
    var slider
    var initialStartValue = +(obj.start)
    var initialEndValue

    if (obj.end == null || undefined) {
        initialEndValue = null
    } else {
        initialEndValue = +(obj.end)
    }

    if (sliderType == 'double') {
        slider = noUiSlider.create(sliderElm, {
            start: [initialStartValue, initialEndValue],
            connect: true,
            step: step,
            range: {
                'min': minValue,
                'max': maxValue
            }
        });

     

        slider.on('update', function (values, handle) {
                       
            var leftV = +(values[0])
            var rightV = +(values[1])
            minValueInput.value = leftV.toFixed()
            maxValueInput.value = rightV.toFixed() 
            
        });

        slider.on('slide', function(values, handle){
            if(handle == 0){
                minValueInput.classList.add('focus')
                maxValueInput.classList.remove('focus')
            } else if (handle == 1){
                maxValueInput.classList.add('focus')
                minValueInput.classList.remove('focus')
            }
        })
        
        minValueInput.addEventListener('change', function () {
            slider.set([this.value])
        })

        maxValueInput.addEventListener('change', function () {
            slider.set([this.value])
        })

    } else if (sliderType == 'single') {
        slider = noUiSlider.create(sliderElm, {
            start: initialStartValue,
            step: step,
            range: {
                'min': minValue,
                'max': maxValue
            }
        });

        minValueInput.value = minValue.toFixed();

        slider.on('update', function () {
            var leftV = +(slider.get())
            minValueInput.value = leftV.toFixed()
           
        });

        slider.on('change', function () {
            if(minValueInput.value != minValue){
                minValueInput.classList.add('active')
            } else {
                minValueInput.classList.remove('active')
            }
        })

        minValueInput.addEventListener('change', function () {
            slider.set([this.value])
        })

        slider.on('slide', function(values, handle){
            if(handle == 0){
                minValueInput.classList.add('focus')
                
            } else if (handle == 1){
                
                minValueInput.classList.remove('focus')
            }
        })
    }


}

export default rangeSlider