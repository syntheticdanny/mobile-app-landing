import './gallery.scss';
// JS
import Swiper from '../../components/swiper/swiper'

// Init all gallery swipers
if (S.find('.gallery-swiper').length > 0) {
    Swiper('.gallery-swiper', {
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
    })
}

// Create class
class Gallery {
    constructor(parent){
        let self = this;
        this.parent = parent;
        this.modalSwiperElm = this.parent.querySelector('.modal .swiper-container')
        this.modalElm = this.parent.querySelector('.modal')

        this.parent.addEventListener('click', function (e) {
            let target = e.target.closest('.gallery__item')
            if (target) {
                self.showGallery( +target.getAttribute('data-index') )
            }
        })
    }
    showGallery(index){
        this.modalElm.wsModal.openModal()
        this.modalSwiperElm.swiper.slideTo(index)
    }
}


if (S.find('.gallery').length > 0) {
    S.each('.gallery', function() {
        new Gallery(this)
    })
}

