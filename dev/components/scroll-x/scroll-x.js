import './scroll-x.scss';
import '../../../node_modules/gsap/ScrollToPlugin'
// TODO: fix formula to scroll elm in center of container

class Scroll {
    constructor(container, options) {
        var self = this;
        var options = options || {}        

        self.container = S.find(container);
        S.on(self.container, 'click', this.scrollX.bind(self))

        Object.assign(this, {
            breackpoint: 1200,
            speed: 1
        })

        Object.assign(this, options)
    }

    scrollX(e) {
        if (window.innerWidth <= this.breackpoint) {
            var scrollElm = e.target.closest('.scroll-x__item')
            
            if (scrollElm) {
                TweenMax.to(this.container[0], this.speed, {
                    scrollTo: {
                        x: scrollElm.offsetLeft - (this.container[0].offsetWidth - scrollElm.offsetWidth) / 2
                    },
                    ease: Power2.easeInOut
                });
            }
        }
    }

    scrollTest(){
        
    }
}

if(S.find('.scroll-x').length > 0){

    S.each('.scroll-x', function () {
    
        var f = new Scroll(this, {
            breackpoint: 991,
            speed: 0.5
        })

    })
}
