// CSS
import './lib.scss';

import '../../components/form/form';
import '../../components/accordion/accordion';
import '../../components/scroll-x/scroll-x'
import '../../components/tab/tab'

import Swiper from '../../components/swiper/swiper';

// JS
var btnCopy = document.querySelectorAll('.code-copy');
btnCopy.forEach(function(btn) {
	btn.addEventListener('click', copyOnClick);
});

function copyOnClick() {
	var mixinCont = this.closest('.lib__actions').querySelector('pre');
	console.log(mixinCont);
	window.getSelection().selectAllChildren(mixinCont);
	document.execCommand('copy');
}

// Swiper init
var libSwiper1 = new Swiper('#lib-swiper-1', {
	autoHeight: true,
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev'
	},
	pagination: {
		el: '.swiper-pagination',
		type: 'bullets',
		clickable: true
	}
});

S('.form').on('submit', function (e) {
    e.preventDefault();
    let thisForm = window.App.Validation(this, {prealoder: true})
})