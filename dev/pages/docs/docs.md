#Utils.js

#### Usage
**HTML**
```html
    <button class="btn">Click</button>
    <button class="btn btn--primary">Click</button>
```

**JS**
```js
    // Find and save elemtns. Adding class
    let btns = S.find('button')
    S.addClass(btns, 'active')

    // Or
    S.addClass('button', 'active')
```

**Result**
```html
    <button class="btn active">Click</button>
    <button class="btn btn--primary active">Click</button>
```

##Methods
#### Find
```js
    let btns = S.find('button')

    // log: [button, button]
```
#### First
Return one element
```js
    let btns = S.first('button')

    // log: [button]
```